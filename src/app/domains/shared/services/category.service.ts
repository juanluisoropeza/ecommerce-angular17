import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Category } from '@shared/models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private http = inject(HttpClient);
  url = 'https://api.escuelajs.co/api/v1/categories';

  getAll() {
    return this.http.get<Category[]>(this.url);
  }
}
