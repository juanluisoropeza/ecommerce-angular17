import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private http = inject(HttpClient);
  url = 'https://api.escuelajs.co/api/v1/products';

  getProducts(category_id?: string) {
    const urlEdited = new URL(this.url);
    if(category_id) {
      urlEdited.searchParams.set('categoryId', category_id);
    }
    return this.http.get<Product[]>(urlEdited.toString());
  }

  getProduct(id: string) {
    return this.http.get<Product>(this.url+'/'+id);
  }
}
