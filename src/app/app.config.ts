import { ApplicationConfig } from '@angular/core';
import { PreloadAllModules, provideRouter, withComponentInputBinding, withPreloading } from '@angular/router';

import { routes } from './app.routes';
import { provideHttpClient } from '@angular/common/http';

// withComponentInputBinding es para que los parametros por url lleguen como inputs a los componentes
// withPreloading se usa para que angular vaya cargando los chunks cuando el usuario haya cargado la pagina 
// y no este generando request a la app, asi mejoramos el performance, esta tecnica se llama Prefetching

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes, withComponentInputBinding(), withPreloading(PreloadAllModules)),
    provideHttpClient()
  ]
};
