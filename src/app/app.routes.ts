import { Routes } from '@angular/router';
import { NotFoundComponent } from '@info/pages/not-found/not-found.component';
import { LayoutComponent } from '@shared/components/layout/layout.component';

// layout maneja lo q se llama vistas anidadas
// esto -> loadComponent: () => import('@products/pages/list/list.component').then(m => m.ListComponent)
// se usa para generar un chunk y el contenido se suba de forma dinamica usando Lazy Loading y Code Splitting
// solo carga en el navegador los chunks declarados lo q mejora el rendimiento de la pag y carga mas rapido
// el about no tiene la promesa por que el componente esta declarado como export default

export const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                loadComponent: () => import('@products/pages/list/list.component').then(m => m.ListComponent)
            },
            {
                path: 'about',
                loadComponent: () => import('@info/pages/about/about.component')
            },
            {
                path: 'product/:id',
                loadComponent: () => import('@products/pages/product-detail/product-detail.component').then(m => m.ProductDetailComponent)
            }
        ]
    },    
    {
        path: '**',
        component: NotFoundComponent
    },
    
];
